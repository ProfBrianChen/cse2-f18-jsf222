// Josh Feinstein Lab03
// Program uses scanner class to help group of friends split a check evenly depending on original cost, percent tip, and number of people splitting
import java.util.Scanner;

public class Check {
  public static void main (String[] args){
Scanner myScanner = new Scanner (System.in);

    System.out.print ( "Enter the original cost of the check in the form xx.xx:" );
      Double checkCost = myScanner.nextDouble ();
      System.out.print ("Enter the percent tip you wish to pay as a whole number (in the form xx)");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //we want to convert the percentage to a decimal value
      System.out.print ("Enter the amount of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    double totalCost;
    double costPerPerson;
    int dollars, // whole dollar amount of cost
    dimes, pennies; // for storing cost
    // to the right of the decimal point
    // for the cost$
    totalCost = checkCost * (1+tipPercent);
    costPerPerson = totalCost / numPeople;
     dollars = (int)(costPerPerson);
     // get the whole amount, dropping decimal fraction dollars=(int)costPerPerson;
      // get dimes amount e.g.,
      // (int) (6.73*10) % 10 -> 67 % 10 -> 7 
      // where the % (mod) operator returns the remainder
      // after the division 583 % 100 -> 83, 27 % 5 -> 2
      dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 10) % 10;
    System.out.println ("Each person in the group owes $" + dollars + "." + dimes + pennies);
    
  }
}