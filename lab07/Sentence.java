//Josh Feinstein CSE002 lab07
//

import java.util.Scanner; //imports the Scanner class
import java.util.Random; //imports the Random class
public class Sentence{ 
  
  public static String adjectives (){ //sets up class for adjectives
    Random myRandom = new Random();
    int x = myRandom.nextInt(10); //random int to run the different cases
    String adj = "brown ";
    switch (x){ //randomly generated number now corresponds to a random adj. 
      case 1: adj = "small ";   
        break;
        
      case 2: adj = "angry ";
        break;
      
      case 3: adj = "gender non-conforming ";        
        break;
      
      case 4: adj = "round ";      
        break;
      
      case 5: adj = "big ";
        break;
      
      case 6: adj = "silly ";
        break;
        
      case 7: adj = "independent ";
        break;
      
      case 8: adj = "mobile ";
        break;
      
      case 9: adj = "handsome ";
        break;
    }
    
  }
  
  public static String subjects (){
    Random myRandom = new Random();
    int y = myRandom.nextInt(10);
    String subj = "";
    switch (y){
    case 1: subj = "boy ";   
        break;
        
      case 2: subj = "fox ";
        break;
      
      case 3: subj = "man ";       
        break;
      
      case 4: subj = "girl ";       
        break;
      
      case 5: subj = "woman ";
        break;
      
      case 6: subj = "dog ";
        break;
        
      case 7: subj = "cat ";
        break;
      
      case 8: subj = "snake ";
        break;
      
      case 9: subj = "person ";  
        break;
  }
  }
  
  public static String verbs (){
    Random myRandom = new Random();
    int z = myRandom.nextInt(10);
    String verb = "";
    switch (z){
    case 1: verb = "jumped ";   
        break;
        
      case 2: verb = "slept "; 
        break;
      
      case 3: verb = "murdered ";       
        break;
      
      case 4: verb = "read ";       
        break;
      
      case 5:verb = "ate ";
        break;
      
      case 6: verb = "missed ";
        break;
        
      case 7: verb = "relaxed ";
        break;
      
      case 8: verb = "destroyed ";
        break;
      
      case 9: verb = "declared "; 
        break;
      }
    }
  
  public static String objects (){
    Random myRandom = new Random();
    int w = myRandom.nextInt(10);
    String obj = "";
    switch (w){
    case 1: obj = "bowl ";   
        break;
        
      case 2: obj = "glass ";
        break;
      
      case 3: obj = "tree ";
        break;
      
      case 4: obj = "branch";        
        break;
      
      case 5: obj = "log ";
        break;
      
      case 6: obj = "house ";
        break;
        
      case 7: obj = "bag";
        break;
      
      case 8: obj = "mouse ";
        break;
      
      case 9: obj = "computer "; 
        break;
    }
  }
  public static void main (String [] args){
    Scanner scn = new Scanner (System.in);
    System.out.println("Would you like to make a sentence? Type '1' for 'Yes', and '2' for 'No'.");
    int response = scn.nextInt();
    if (response == 1){
      System.out.println()
    }
    
    
  }
}