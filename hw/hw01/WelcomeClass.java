// Josh Feinstein CSE02
// Homework #1
// Purpose: Create a message which welcomes the class
public class WelcomeClass{
  public static void main(String args[]){
    //Display "Welcome Class message"
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println(" ^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--S--F--2--2--2->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println(" v  v  v  v  v  v");
    System.out.println("Hello, my name is Josh Feinstein and I am from Needham, MA. I like to play volleyball and eat food. I am a student in the business school.");
  }
}