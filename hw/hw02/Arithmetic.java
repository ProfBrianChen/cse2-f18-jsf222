// Josh Feinstein CSE002 HW02

//Program will be able to manipulate data stored in variables and print out resulting numerical results from simple calculations

public class Arithmetic {
  public static void main (String []args) {
   int numPants = 3; //number of pairs of pants purchased 
    double pantsPrice = 34.98; //price of one pair of pants
      int numShirts = 2; //number of sweatshirts purchased
      double shirtsPrice = 24.99; //price of one sweatshirt
    int numBelts = 1; //number of belts purchased
    double beltCost = 33.99; //price of one belt
    double paSalesTax = .06; //the tax rate in PA
    double totalPantsPrice = numPants * pantsPrice,
    totalShirtsPrice = numShirts * shirtsPrice,
    totalBeltCost = numBelts * beltCost; //these are the prices of each category of items
    double totalCost = totalPantsPrice + totalShirtsPrice + totalBeltCost, //total cost of all items
    totalTax = paSalesTax * totalCost, //amount of tax payed with this purchase
    totalCostWithTax = totalCost + totalTax; //total price including tax added 
    System.out.println ("The total cost of pants was $" + totalPantsPrice);
    System.out.println ("The total cost of sweatshirts was $" + totalShirtsPrice);
    System.out.println ("The total cost of belts was $" + totalBeltCost);
    System.out.println ("The total cost of the clothes without tax was $" + totalCost);
    System.out.printf ("The total additional cost of tax was $" + "%.2f" , totalTax);
    System.out.printf ("%n" + "The total amount paid in this transaction, including tax, was $" + "%.2f", totalCostWithTax);
  }
}
