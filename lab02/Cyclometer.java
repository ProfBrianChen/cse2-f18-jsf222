// Josh Feinstein CSE002 Lab02

//Program is designed to count rotations of tire on bike or the distance traveled and give back information on trip.
public class Cyclometer {
  //Main method of Java Program
  public static void main (String []args) {
    int secsTrip1=480; //This is input on the amount of time spent riding the bike in Trip 1
    int secsTrip2=3220; //This is input on the amount of time spent riding the bike in Trip 2
    int countsTrip1=1561; //This is input on the amount of times the wheel completed a full rotation on Trip 1
    int countsTrip2=9037; //This is input on the amount of times the wheel completed a full rotation on Trip 2
    double wheelDiameter=27.0, //Diameter of the wheel, use to find the wheel's circumference to help with distance traveled
    PI=3.14159, //PI is a constant used to help find the circumference of a circle, will help find distance traveled
    feetPerMile=5280, //Will be used to convert the rotations of the wheel into miles traveled after it has been converted into feet traveled
    inchesPerFoot=12, //Will be used to convert the wheel circumference into feet traveled by multiplying by the number of rotations
    secondsPerMinute=60; //Will be used to find the duration of each trip in minutes
    double distanceTrip1, distanceTrip2, totalDistance;
    System.out.println ("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes, and had " 
                        + (countsTrip1) + " counts." );
    System.out.println ("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes, and had "  
                        + (countsTrip2) + " counts." );
   //PI*wheelDiameter=circumference, circumference*countsTrip1=distanceTrip1, circumference*countsTrip2=distanceTrip2, 
    //distanceTrip1+distanceTrip2=totalDistance
    //3.14159*27.0=84.82293, 84.82293*1561=132408.594, 84.82293*766544.818, 
    //132408.594+766544.818=898953.412
    distanceTrip1 = (((PI * wheelDiameter * countsTrip1)/inchesPerFoot)/ feetPerMile); //Gives distance in miles for Trip 1
    distanceTrip2 = (((PI * wheelDiameter * countsTrip2)/inchesPerFoot)/ feetPerMile); //Give distance in miles for Trip 2
    totalDistance = distanceTrip1 + distanceTrip2;
   System.out.println ("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println ("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println ("The total distance of the two trips was " + totalDistance + " miles");
  }
    
} //end of class