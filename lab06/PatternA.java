//Josh Feinstein CSE002 Lab06 part A 
// October 11, 2018
// Program will:
// 1. Ask for input
// 2. Use input to create that many lines
import java.util.Scanner;

public class PatternA {
  public static void main (String[] args){
    Scanner myScanner = new Scanner (System.in);
    boolean given = false; //used to initiate loop
 // introduces variables
    int lines = 0; //int variable for number of lines, intitializes variable
    System.out.println ("Please enter an integer 1-10 to decide how many rows to create."); //prompts user
    //Makes sure the user inputs an int variable
    while (given==false){ //initiates loop
     if (myScanner.hasNextInt()){ //
       lines = myScanner.nextInt ();//user inputs how many lines they want for the pattern
       given = true;
    }
      else { // When if statement isn't true this will prompt the user again and ask for an in
        System.out.println ("Error");
        System.out.println ("You did not enter an integer, please now enter an integer 1-10. ");
        myScanner.next();
      }
    }
    for(int numRows = 1; numRows <= lines; numRows++){ //For loop which establishes number of rows
      for ( int numCols = 1; numCols <= numRows ; numCols++){ //For loop which sets the number of columns to match the row number
       System.out.print (numCols + " "); //prints out row of numbers
      //
      
      }
      System.out.println ();
 
    }
  }
}