// Josh Feinstein CSE002 HW09 Final


import java.util.*;

public class RemoveElement{
  
   public static void main(String [] arg){
     
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
  
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
  public static int[] randomInput(){
    int [] a1 = new int [10]; //allocates array
     for (int i = 0; i < 10; i++){ //for loop which fills in array with random integers 0-99
      int rand = (int)(Math.random() * 10); // Creates random number for array
      a1[i] = rand;
  }
    return a1;
}
 
  public static int[] delete(int[] list , int pos){
    int[] newList = new int [9]; //creates new array
    if (pos > list.length){ //when desired postion is greater than length it is invalid
        System.out.println("The index is not valid.");
      return list;
      }
    for (int i = 0; i < pos; i++){
      newList[i] = list[i];
    }
    for (int i = 0; i < 9; i++){
     if ( i == pos){
      newList[i] = list[i + 1];
     }
    }
      for (int i = pos + 1; i < 9; i++){
        newList[i] = list [i+1];
      }
        return newList;

    }
    
  
  
  public static int[] remove(int[] list, int target){
    int counter = 0; //creates counter for the number of occurences of target element
    for (int i = 0; i < list.length; i++){
      if (list[i] == target){counter++;}
    }
    if (counter == 0){
      System.out.println("Element " + target + " was not found");
      return list;
    }
    else{
      System.out.println("Element " + target + " was found.");
    }
    int [] newList = new int [list.length - counter];
    int counter2 = 0;
    for (int k = 0; k < list.length; k++){
      if (list[k] != target){
      newList[counter2] = list[k];
        counter2++; 
        
     }
      
    }
   
    return newList;
  }
}  

