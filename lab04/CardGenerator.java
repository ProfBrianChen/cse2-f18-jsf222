// Josh Feinstein CSE002 LAB04
// Purpose of this lab is to create a program which will randomly select 1 of 52 cards and produce the name of the card

// First is set of if else statements, if cardNum is in range then assign the "String" of the suit to corresponding suit name
//Second is switch statement which takes cardNum modulus 13, 

public class CardGenerator {
  public static void main (String[] args){
    int cardNum = (int)(Math.random()*51)+1;
    String cardName = "";
    if (cardNum <=13){ //This will determine the suit to be diamonds if the number generated is less than or equal to 13
      if (cardNum==1){ 
      cardName = "Ace"; //This line will name the card as an Ace
      }
      if (cardNum >= 2 && cardNum <= 10){ //Determines that the name of the card will just be it's number value
        cardName = "" + cardNum;
      }
   if (cardNum==11){ //Selects which number corresponds to the Jack of diamonds
     cardName = "Jack"; //Gives name of card
   }
    if (cardNum==12){ //Selects which number corresponds to the Queen of Diamonds
      cardName = "Queen"; //Gives name of card
    }
      if (cardNum==13){ //Selects which number corresponds to the King of Diamonds
        cardName = "King"; //Gives name of card
      }
  System.out.println ("You picked the " + cardName + " of Diamonds");
    }
    
  if (cardNum>13 && cardNum <=26){ //This will determine the suit to be CLubs if the number generated is between 14 and 26
      if (cardNum==14){ 
      cardName = "Ace"; //This line will name the card as an Ace
      }
      switch (cardNum){ //Determines that the name of the card will just be it's number value
        case 15: cardName = "" + 15%13; //Must adjust so that the given value is between 2 and 10
          break;
        case 16: cardName = "" + 16%13;
          break;
        case 17: cardName = "" + 17%13;
          break;
        case 18: cardName = "" + 18%13;
          break;
        case 19: cardName = "" + 19%13;
          break;
        case 20: cardName = "" + 20%13;
          break;
        case 21: cardName = "" + 21%13;
          break;
        case 22: cardName = "" + 22%13;
          break;
        case 23: cardName = "" + 23%13;
          break;
      }
   if (cardNum==24){ //Selects which number corresponds to the Jack of Clubs
     cardName = "Jack"; //Gives name of card
   }
    if (cardNum==25){ //Selects which number corresponds to the Queen of Clubs
      cardName = "Queen"; //Gives name of card
    }
      if (cardNum==26){ //Selects which number corresponds to the King of Clubs
        cardName = "King"; //Gives name of card
      }
  System.out.println ("You picked the " + cardName + " of Clubs");  }
    
     if (cardNum>27 && cardNum <=39){ //This will determine the suit to be Hearts if the number generated is between 27 and 39
      if (cardNum==27){ 
      cardName = "Ace"; //This line will name the card as an Ace
      }
      switch (cardNum){ //Determines that the name of the card will just be it's number value
        case 28: cardName = "" + 28%13; //Must adjust so that the given value is between 2 and 10
          break;
        case 29: cardName = "" + 29%13;
          break;
        case 30: cardName = "" + 30%13;
          break;
        case 31: cardName = "" + 31%13;
          break;
        case 32: cardName = "" + 32%13;
          break;
        case 33: cardName = "" + 33%13;
          break;
        case 34: cardName = "" + 34%13;
          break;
        case 35: cardName = "" + 35%13;
          break;
        case 36: cardName = "" + 36%13;
          break;
      }
   if (cardNum==37){ //Selects which number corresponds to the Jack of Hearts
     cardName = "Jack"; //Gives name of card
   }
    if (cardNum==38){ //Selects which number corresponds to the Queen of Hearts
      cardName = "Queen"; //Gives name of card
    }
      if (cardNum==39){ //Selects which number corresponds to the King of Hearts
        cardName = "King"; //Gives name of card
      }
  System.out.println ("You picked the " + cardName + " of Hearts");  }
    
     if (cardNum>40 && cardNum <=52){ //This will determine the suit to be Spades if the number generated is between 40 and 52
      if (cardNum==40){ 
      cardName = "Ace"; //This line will name the card as an Ace
      }
      switch (cardNum){ //Determines that the name of the card will just be it's number value
        case 41: cardName = "" + 41%13; //Must adjust so that the given value is between 2 and 10
          break;
        case 42: cardName = "" + 42%13;
          break;
        case 43: cardName = "" + 43%13;
          break;
        case 44: cardName = "" + 44%13;
          break;
        case 45: cardName = "" + 45%13;
          break;
        case 46: cardName = "" + 46%13;
          break;
        case 47: cardName = "" + 47%13;
          break;
        case 48: cardName = "" + 48%13;
          break;
        case 49: cardName = "" + 49%13;
          break;
      }
   if (cardNum==50){ //Selects which number corresponds to the Jack of Spades
     cardName = "Jack"; //Gives name of card
   }
    if (cardNum==51){ //Selects which number corresponds to the Queen of Spades
      cardName = "Queen"; //Gives name of card
    }
      if (cardNum==52){ //Selects which number corresponds to the King of Spades
        cardName = "King"; //Gives name of card
      }
  System.out.println ("You picked the " + cardName + " of Spades");  }
}}
