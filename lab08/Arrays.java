// Josh Feinstein CSE002 Lab08
// Arrays lab purpose:
// Create 2 array of integers with each having a size of 100.
// Fill in one of the arrays with randomized integers in the range of 0 to 99, using math.random().
// Use the second array to hold the number of occurrences of each number in the first array. 


import java.util.*;

public class Arrays{
  public static void main (String [] args){
    
    final int numNums = 100; //Declares variable for allocating the array
    int a1[]; //Declares first array
    int a2[]; //Declares second array   
    a1 = new int [numNums]; //allocates array 1
    a2 = new int [numNums]; // allocates array 2
    for (int i = 0; i < numNums; i++){ //for loop which fills in array with random integers 0-99
      int rand = (int)(Math.random() * 100); // Creates random number for array
      a1[i] = rand;
      a2[rand] += 1;
      System.out.print(rand + ",");
    }
  System.out.println();
    for (int j = 0; j < 100; j++){
      
      System.out.println(j + " occurs " + a2[j] + " times.");
    }
    
    
    
  }
}
