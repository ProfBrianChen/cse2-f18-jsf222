// Josh Feinstein CSE002 HW06
// Purpose: 
// Display a secret X inside the *'s
import java.util.Scanner;

public class EncryptedX{
  public static void main (String []args){
    boolean given =  false;
    int r = 1; // Introduces variable
    Scanner myScanner = new Scanner (System.in);
    System.out.println ("How many rows would you like? 0-100"); //Prompts for number of rows
    while (given==false){ //initiates loop
     if (myScanner.hasNextInt()){ //
       r = myScanner.nextInt ();//user inputs how many lines they want for the pattern
       given = true;
    }
      else { // When if statement isn't true this will prompt the user again and ask for an in
        System.out.println ("Error");
        System.out.println ("You did not enter an integer, please now enter an integer 0-100. ");
        myScanner.next();
      }
    }
            int k = 0; //introduces variable
    
    for (int i = 0; i<=r; i++){ // Outer loop makes correct number of rows
      for (int j = 0; j<r; j++) {      //Changes what is put in each row
       if (j == k){ //Prints out a space when 
         System.out.print(" "); 
       }
        if (j == r - k){
          System.out.print(" "); //Changes what is printed when rows and columns are equal
        }
      
          System.out.print("*"); //Prints * when "if" conditions aren't satisfied
       
      }
      System.out.println();
      k++;
    }
  }
}