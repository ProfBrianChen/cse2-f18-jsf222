// Josh Feinstein CSE002 HW04 part one. If/Else statements only. 
//The purpose of this program is to: 
// 1. Ask the user if they want to roll the dice or input values of the rollen dice themselves
// 2. If they are rolling the program will then generate two random integers between 1 and 6
// 3. If they decide to input the values of the dice themselves, using a scanner program they will be prompted to input the dice. 
// 4. The program will then determine the slang terminology of the combination of the rollen dice. 

// This program will only be using If/Else statements, no Switch. 
import java.util.Scanner;
public class CrapsIf {
  public static void main (String[] args){
    Scanner myScanner = new Scanner (System.in);
    
    System.out.println ("Would you like to roll the dice yourself? Or would you rather supply two dice values yourself? "); //Prompts the user on whether they want to be given randomly generated numbers
    System.out.println ("Please reply with either '1' for 'Rolling Yourself' or '2' to 'Supply two dice values'. "); // or if they want to input the values themselves
    int rollingDice = myScanner.nextInt (); // This allows the user to input which choice they want Roll random dice or supply dice values
    if (rollingDice == 1) {
      int diceOne = (int)(Math.random() * 5)+1; //These two lines generate the random dice values for the user
      int diceTwo = (int)(Math.random() * 5)+1;
      if (diceOne == 1){ //Depending on what the First Value is of the randomly generated values the program will then print out a line depending on what the second generated value is 
        if (diceTwo == 1){ 
          System.out.println ("You rolled Snake Eyes");
 }
        if (diceTwo == 2){
          System.out.println ("You rolled an Ace Deuce");
 }
    
      if (diceTwo == 3){
          System.out.println ("You rolled an Easy Four");
}
      if (diceTwo == 4){
          System.out.println ("You rolled a Fever Five");
}
      if (diceTwo == 5){
          System.out.println ("You rolled an Easy Six");
}
      if (diceTwo == 6){
          System.out.println ("You rolled a Seven Out");
}
      }
      if (diceOne == 2){ // Depending on what the random values are there will be different results, this subset of code (lines 41-60) determines the output when the first random value is 2
        if (diceTwo == 1){
          System.out.println ("You rolled an Ace Deuce");
}
        if (diceTwo == 2){
          System.out.println ("You rolled a Hard Four");
 }
        if (diceTwo == 3){
          System.out.println ("You rolled an Fever Five");
 }
        if (diceTwo == 4){
          System.out.println ("You rolled an Easy Six");
 }
        if (diceTwo == 5){
          System.out.println ("You rolled a Seven Out");
 }
        if (diceTwo == 6){
          System.out.println ("You rolled an Easy Eight");
 }
      }
      if (diceOne == 3){
                if (diceTwo == 1){
          System.out.println ("You rolled an Easy Four");
}
        if (diceTwo == 2){
          System.out.println ("You rolled a Fever Five");
 }
         if (diceTwo == 3){
          System.out.println ("You rolled a Hard Six");
 }
         if (diceTwo == 4){
          System.out.println ("You rolled a Seven Out");
 }
         if (diceTwo == 5){
          System.out.println ("You rolled an Easy Eight");
 }
         if (diceTwo == 6){
          System.out.println ("You rolled a Nine");
 }
      }
      if (diceOne == 4){
        if (diceTwo == 1){
          System.out.println ("You rolled a Fever Five");
}
        if (diceTwo == 2){
          System.out.println ("You rolled an Easy Six");
 }
         if (diceTwo == 3){
          System.out.println ("You rolled a Seven Out");
 }
        if (diceTwo == 4){
          System.out.println ("You rolled a Hard Eight");
 }
         if (diceTwo == 5){
          System.out.println ("You rolled a Nine");
 }
         if (diceTwo == 6){
          System.out.println ("You rolled an Easy Ten");
 }
      }
      if (diceOne == 5){
        if (diceTwo == 1){
          System.out.println ("You rolled an Easy Six");
}
        if (diceTwo == 2){
          System.out.println ("You rolled a Seven Out");
 }
         if (diceTwo == 3){
          System.out.println ("You rolled an Easy Eight");
 }
        if (diceTwo == 4){
          System.out.println ("You rolled a Nine");
 }
        if (diceTwo == 5){
          System.out.println ("You rolled a Hard Ten");
 }
         if (diceTwo == 6){
          System.out.println ("You rolled an Yo-Leven");
 }
      }
      if (diceOne == 6){
        if (diceTwo == 1){
          System.out.println ("You rolled a Seven Out");
}
        if (diceTwo == 2){
          System.out.println ("You rolled an Easy Eight");
 }
         if (diceTwo == 3){
          System.out.println ("You rolled a Nine");
 }
        if (diceTwo == 4){
          System.out.println ("You rolled a Easy Ten");
 }
        if (diceTwo == 5){
          System.out.println ("You rolled a Yo-Leven");
 }
        if (diceTwo == 6){
          System.out.println ("You rolled Boxcars");
}
      }
}
    if (rollingDice == 2){
      System.out.println ("Please input the first number you would like in the form of X"); //Prompts user to input values
      int firstDie = myScanner.nextInt();//User inputs the first value they wish to use
      System.out.println ("Please input the second number you would like in the form of X"); //Prompts user to input values
      int secondDie = myScanner.nextInt();//User inputs the second value they wish to use
       if (firstDie == 1){ // Lines 147-271 give outputs depending on what the input values are that come from the user in the previous lines
         if(secondDie == 1){
          System.out.println ("You rolled Snake Eyes");
 }
        if (secondDie == 2){
          System.out.println ("You rolled an Ace Deuce");
 }
    
      if (secondDie == 3){
          System.out.println ("You rolled an Easy Four");
}
      if (secondDie == 4){
          System.out.println ("You rolled a Fever Five");
}
      if (secondDie == 5){
          System.out.println ("You rolled an Easy Six");
}
      if (secondDie == 6){
          System.out.println ("You rolled a Seven Out");
}
      }
      if (firstDie == 2){
        if (secondDie == 1){
          System.out.println ("You rolled an Ace Deuce");
 }
        if (secondDie == 2){
          System.out.println ("You rolled a Hard Four");
 }
        if (secondDie == 3){
          System.out.println ("You rolled an Fever Five");
 }
        if (secondDie == 4){
          System.out.println ("You rolled an Easy Six");
 }
        if (secondDie == 5){
          System.out.println ("You rolled a Seven Out");
 }
        if (secondDie == 6){
          System.out.println ("You rolled an Easy Eight");
 }
      }
      if (firstDie == 3){
         if (secondDie == 1){
          System.out.println ("You rolled an Easy Four");
 }
        if (secondDie == 2){
          System.out.println ("You rolled a Fever Five");
 }
         if (secondDie == 3){
          System.out.println ("You rolled a Hard Six");
 }
         if (secondDie == 4){
          System.out.println ("You rolled a Seven Out");
 }
         if (secondDie == 5){
          System.out.println ("You rolled an Easy Eight");
 }
         if (secondDie == 6){
          System.out.println ("You rolled a Nine");
 }
      }
      if (firstDie == 4){
          if (secondDie == 1){
          System.out.println ("You rolled a Fever Five");
 }
        if (secondDie == 2){
          System.out.println ("You rolled an Easy Six");
 }
         if (secondDie == 3){
          System.out.println ("You rolled a Seven Out");
 }
        if (secondDie == 4){
          System.out.println ("You rolled a Hard Eight");
 }
         if (secondDie == 5){
          System.out.println ("You rolled a Nine");
 }
         if (secondDie == 6){
          System.out.println ("You rolled an Easy Ten");
 }
      }
      if (firstDie == 5){
        if (secondDie == 1){
          System.out.println ("You rolled an Easy Six");
 }
        if (secondDie == 2){
          System.out.println ("You rolled a Seven Out");
 }
         if (secondDie == 3){
          System.out.println ("You rolled an Easy Eight");
 }
        if (secondDie == 4){
          System.out.println ("You rolled a Nine");
 }
        if (secondDie == 5){
          System.out.println ("You rolled a Hard Ten");
 }
         if (secondDie == 6){
          System.out.println ("You rolled an Yo-Leven");
 }
      }
      if (firstDie == 6){
                  if (secondDie == 1){
          System.out.println ("You rolled a Seven Out");
 }
        if (secondDie == 2){
          System.out.println ("You rolled an Easy Eight");
 }
         if (secondDie == 3){
          System.out.println ("You rolled a Nine");
 }
        if (secondDie == 4){
          System.out.println ("You rolled an Easy Ten");
}
        if (secondDie == 5){
          System.out.println ("You rolled a Yo-Leven");
}
        if (secondDie == 6){
          System.out.println ("You rolled Boxcars");
}
      }
      
    }
}
}