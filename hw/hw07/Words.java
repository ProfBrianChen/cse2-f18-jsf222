// Josh Feinstein CSE HW07
// Word Tools
import java.util.Scanner;
public class Words{
  public static void main (String[] args){
    String txt = sampleText(); //Executes sample text method which has the user input a text
    printMenu(txt); //Executes Print Menu  method which prints out options for 
  }
  
  
  public static String sampleText (){
    Scanner myScanner = new Scanner (System.in); //Creates Scanner
    System.out.println("Please enter sample text."); //Has user input sample text
    String txt = myScanner.nextLine(); //Creates string within which sample text will be stored
    System.out.println("You entered: " + txt); // Prints out the users text
    return txt; //Output of method is user input
  }
  public static void printMenu(String txt){
    Scanner myScanner = new Scanner (System.in); //Creates scanner 
    String option = "a"; //Declares variable
    while (!option.equals("q")){
  System.out.println("c - Number of non-whitespace characters");  //User inputs what they desire to with their text
  System.out.println("w - Number of words");  //User inputs what they desire to with their text
  System.out.println("f - Find text");  //User inputs what they desire to with their text
  System.out.println("r - Replace all !'s");  //User inputs what they desire to with their text
  System.out.println("s - Shorten spaces");    //User inputs what they desire to with their text
  System.out.println("q - Quit"); //User inputs that they desire to quit the program
  System.out.println("Choose an option");
     option = myScanner.next(); //User inputs what they desire to do based off the printed list of options
    
    if (option.equals("c")){ //Executes statement which calls methods to go along with what the user desires to do with the text
      System.out.println ("Number of non-whitespace characters: " + getNumOfNonWSCharacters(txt));
    }
    else if (option.equals("w")){ //Executes statement which calls methods to go along with what the user desires to do with the text
      System.out.println ("Number of words: " + getNumWords(txt));
    }
    else if (option.equals("f")){ //Executes statement which calls methods to go along with what the user desires to do with the text
     System.out.println("Please enter a word");
      String word = myScanner.next(); // User inputs what word they are searching for
      System.out.println ("Instances: " + findText(txt , word));
    }
    else if (option.equals("r")){ //Executes statement which calls methods to go along with what the user desires to do with the text
      System.out.println("Edited text: " + replaceExclamation(txt));
    }
    else if (option.equals("s")){ //Executes statement which calls methods to go along with what the user desires to do with the text
      System.out.println ("Edited text: " + shortenSpace(txt));
    }
    else if (option.equals("q")){ //Executes statement which calls methods to go along with what the user desires to do with the text
      System.out.println("Quit");
       }
     }
    return; 
 }
  
  
  public static int getNumOfNonWSCharacters(String txt){
    int chars = txt.length(); //Sets variable equal to total number of characters
    int WS = 0; //Declares variable 
    char uno = 'u'; //Declares variable
    for (int i = 0; i < chars; i++){
      uno = txt.charAt(i);
      if (Character.isWhitespace(uno)){
        WS++; //Adds one to white space counter if character is a white space
      }
    }
    chars = chars - WS; //changes the new number of characters to be the number of total characters minus number of white spaces
    return chars; //returns int with number of Non-whitespace characters
   
  }
  
  public static int getNumWords(String txt){
   int chars = txt.length(); //Sets new variable to equal total length of string
   char uno = 'u'; //Declares variable
    char dos = 'd'; //Declares variable
    int numWords = 0; //Declares variable 
    for (int i = 0; i< chars; i++){ //For loop which will continue until all characters have been through it
      uno = txt.charAt(i); //Sets uno to be whatever character is found at spot i.
      dos = txt.charAt(i-1); //Sets dos to be whatever character is found at the spot coming before spot i.
      if (Character.isLetter(uno) && Character.isWhitespace(dos)){
        numWords++; //increases the counter for number of words by 1.
      }
    }
    return numWords;
  }
  
  public static int findText(String txt, String word){
    int x = txt.length(); //Sets new variable to equal total length of text from user input including whitespaces
    int c = word.length(); //Sets new variable to equal length of word being searched for
    int wordAppearances = 0; //Declares variable which will be given back to user with number of appearances
    int charShared = 0; //Declares Variable for how many characters are the same between the word being processed and word searched for 
    char txtChar = 'a'; //Declares Variable for what the character being looked at in the txt is
    char wordChar = 's'; //Declares Variable for what the character in the word being searched for is
    int j = 0; //Declares variable for matching characters between word in txt and word being searched for
   for (int i = 0; i < x; i++){
     txtChar = txt.charAt(i); //sets the char to be equal the number of times the loop has been undergone
     wordChar = txt.charAt(j); //
     if (txtChar == wordChar){ //If statement for when the character observed in both words are the same
       charShared++; //Adds one to counter of letters in commmon between words
       j++; //Moves on to next letter in word being searched for
       
     }
     if (charShared == c){ //If statement for when the characters match at every spot of the word
       wordAppearances++; //Adds one to counter for number of appearances of the word
       
     }
   }
    return wordAppearances; //Returns the number of times the word appears
  }
  
  public static String replaceExclamation(String txt){ //method replaces exclamation points with periods
    String txt2 = txt.replace('!' , '.'); //Replaces all exclamation points with periods
    return txt2; //returns the new text with all exclamation points now being periods
  }
  
  public static String shortenSpace(String txt){ //method replaces doubles spaces with single spaces
    int chars = txt.length(); //Declares variable 
    char txtChar = 'a'; //Declares variable
    char txtChar2 = 's'; //Declares variable
    String txt2 = " "; //Declares new string which will be the edited text
    for (int i = 0; i < chars--; i++){
      txtChar = txt.charAt(i); //Sets the character being looked at to the number of times going through the loop
      txtChar2 = txt.charAt(i+1); //Sets the character being looked at to the number of times going through the loop plus one 
      if (Character.isWhitespace(txtChar) && Character.isWhitespace(txtChar2)){
        txt2 = txt.replace("  " , " "); //Replaces double spaces with single spaces
      }
      
    }
    return txt2; //Returns new edited text without double spaces
  }
  
  

}