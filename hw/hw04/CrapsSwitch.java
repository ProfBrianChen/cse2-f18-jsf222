// Josh Feinstein CSE002 HW04 part two. Switch statements only. 
//The purpose of this program is to: 
// 1. Ask the user if they want to roll the dice or input values of the rollen dice themselves
// 2. If they are rolling the program will then generate two random integers between 1 and 6
// 3. If they decide to input the values of the dice themselves, using a scanner program they will be prompted to input the dice. 
// 4. The program will then determine the slang terminology of the combination of the rollen dice. 

// This program will only be using Switch commands. 

import java.util.Scanner;
public class CrapsSwitch {
  public static void main (String[] args){
    Scanner myScanner = new Scanner (System.in);
   
    System.out.println ("Would you like to roll the dice yourself? Or would you rather supply two dice values yourself? ");//Prompts the user on whether they want to be given randomly generated numbers
    System.out.println ("Please reply with either '1' for 'Rolling Yourself' or '2' to 'Supply two dice values'. "); // or if they want to input the values themselves
    int rollingDice = myScanner.nextInt ();  // This allows the user to input which choice they want Roll random dice or supply dice values
    switch (rollingDice){
      case 1:
        int diceOne = (int)(Math.random() * 5)+1;
      int diceTwo = (int)(Math.random() * 5)+1;
        switch (diceOne){ //Depending on what the First Value is of the randomly generated values the program will then print out a line depending on what the second generated value is 
          case 1: 
            switch(diceTwo){
              case 1: System.out.println ("You rolled Snake Eyes");
                break;
              case 2: System.out.println ("You rolled an Ace Deuce");
                break;
              case 3: System.out.println ("You rolled an Easy Four");
                break;
              case 4: System.out.println ("You rolled a Fever Five");
                break;
              case 5: System.out.println ("You rolled an Easy Six");
                break;
              case 6: System.out.println ("You rolled a Seven Out");
                break;
            }break;
          case 2: 
            switch(diceTwo){
              case 1: System.out.println ("You rolled an Ace Deuce");
                break;
              case 2: System.out.println ("You rolled a Hard Four") ;
                break;
              case 3: System.out.println ("You rolled an Fever Five");
                break;
              case 4: System.out.println ("You rolled an Easy Six");
                break;
              case 5: System.out.println ("You rolled a Seven Out");
                break;
              case 6: System.out.println ("You rolled an Easy Eight");
                break;
            }break;
          case 3:
            switch(diceTwo){
                case 1: System.out.println ("You rolled an Easy Four");
                break;
              case 2: System.out.println ("You rolled a Fever Five") ;
                break;
              case 3: System.out.println ("You rolled a Hard Six");
                break;
              case 4: System.out.println ("You rolled a Seven Out");
                break;
              case 5: System.out.println ("You rolled an Easy Eight");
                break;
              case 6: System.out.println ("You rolled a Nine");
                break;
            }break;
          case 4:
            switch(diceTwo){
              case 1: System.out.println ("You rolled a Fever Five");
                break;
              case 2: System.out.println ("You rolled an Easy Six") ;
                break;
              case 3: System.out.println ("You rolled a Seven Out");
                break;
              case 4: System.out.println ("You rolled a Hard Eight");
                break;
              case 5: System.out.println ("You rolled a Nine");
                break;
              case 6: System.out.println ("You rolled an Easy Ten");
                break;
            }break;
          case 5:
            switch(diceTwo){
               case 1: System.out.println ("You rolled an Easy Six");
                break;
              case 2: System.out.println ("You rolled a Seven Out") ;
                break;
              case 3: System.out.println ("You rolled an Easy Eight");
                break;
              case 4: System.out.println ("You rolled a Nine");
                break;
              case 5: System.out.println ("You rolled a Hard Ten");
                break;
              case 6: System.out.println ("You rolled an Yo-Leven");
                break;
            }break;
          case 6:
            switch(diceTwo){
              case 1: System.out.println ("You rolled a Seven Out");
                break;
              case 2: System.out.println ("You rolled an Easy Eight") ;
                break;
              case 3: System.out.println ("You rolled a Nine");
                break;
              case 4: System.out.println ("You rolled an Easy Ten");
                break;
              case 5: System.out.println ("You rolled a Yo-Leven");
                break;
              case 6: System.out.println ("You rolled Boxcars");
                break;
            }break;
       } break;
      case 2:
      System.out.println ("Please input the first number you would like in the form of X"); //Prompts user to input values
      int firstDie = myScanner.nextInt(); //User inputs first value
      System.out.println ("Please input the second number you would like in the form of X"); //Prompts user to input vales
      int secondDie = myScanner.nextInt(); //User inputs second value
      switch(firstDie){ 
        case 1:
            switch(secondDie){
              case 1: System.out.println ("You rolled Snake Eyes");
                break;
              case 2: System.out.println ("You rolled an Ace Deuce");
                break;
              case 3: System.out.println ("You rolled an Easy Four");
                break;
              case 4: System.out.println ("You rolled a Fever Five");
                break;
              case 5: System.out.println ("You rolled an Easy Six");
                break;
              case 6: System.out.println ("You rolled a Seven Out");
                break;
            }
          case 2: 
            switch(secondDie){
              case 1: System.out.println ("You rolled an Ace Deuce");
                break;
              case 2: System.out.println ("You rolled a Hard Four") ;
                break;
              case 3: System.out.println ("You rolled an Fever Five");
                break;
              case 4: System.out.println ("You rolled an Easy Six");
                break;
              case 5: System.out.println ("You rolled a Seven Out");
                break;
              case 6: System.out.println ("You rolled an Easy Eight");
                break;
            }
          case 3:
            switch(secondDie){
                case 1: System.out.println ("You rolled an Easy Four");
                break;
              case 2: System.out.println ("You rolled a Fever Five") ;
                break;
              case 3: System.out.println ("You rolled a Hard Six");
                break;
              case 4: System.out.println ("You rolled a Seven Out");
                break;
              case 5: System.out.println ("You rolled an Easy Eight");
                break;
              case 6: System.out.println ("You rolled a Nine");
                break;
            }
          case 4:
            switch(secondDie){
              case 1: System.out.println ("You rolled a Fever Five");
                break;
              case 2: System.out.println ("You rolled an Easy Six") ;
                break;
              case 3: System.out.println ("You rolled a Seven Out");
                break;
              case 4: System.out.println ("You rolled a Hard Eight");
                break;
              case 5: System.out.println ("You rolled a Nine");
                break;
              case 6: System.out.println ("You rolled an Easy Ten");
                break;
            }
          case 5:
            switch(secondDie){
              case 1: System.out.println ("You rolled an Easy Six");
                break;
              case 2: System.out.println ("You rolled a Seven Out") ;
                break;
              case 3: System.out.println ("You rolled an Easy Eight");
                break;
              case 4: System.out.println ("You rolled a Nine");
                break;
              case 5: System.out.println ("You rolled a Hard Ten");
                break;
              case 6: System.out.println ("You rolled an Yo-Leven");
                break;
            }
          case 6:
            switch(secondDie){
              case 1: System.out.println ("You rolled a Seven Out");
                break;
              case 2: System.out.println ("You rolled an Easy Eight") ;
                break;
              case 3: System.out.println ("You rolled a Nine");
                break;
              case 4: System.out.println ("You rolled an Easy Ten");
                break;
              case 5: System.out.println ("You rolled a Yo-Leven");
                break;
              case 6: System.out.println ("You rolled Boxcars");
                break;
            }
        break;}
    }
  }
}