// Josh Feinstein CSE02, HW03 part 1 9/17/18
// Program will ask user for input data on number of acres of land affected by the hurricanes.
// Program will then ask for how many inches of rain fell on average and then using the two inputs will find the number of cubic miles of rain. 
import java.util.Scanner;
public class Convert {
  public static void main (String[] args){
Scanner myScanner = new Scanner (System.in);
    
    System.out.print ("Enter the affected area in acres in the form of xx.xx"); //asks for input from user
      Double areaAffected = myScanner.nextDouble (); //produces Double for area affected in acres
    System.out.print ("Enter the average number of inches of rain across the area in the form of xx.xx"); //asks for input from user
    Double inchesWater = myScanner.nextDouble (); //produces Double for average number of inches of rain
    // 1 inch of rain over 1 acre is equal to around 27154 gallons
    double gallonsWater = areaAffected *inchesWater;
    // 1101117147428.57gal = 1 cubic mile
    double WaterCuMi = gallonsWater * 1101117147428.57;
    System.out.println ("The amount of rain that fell is equal to " + WaterCuMi + " Cubic Miles");
    
  }
}
