//Josh Feinstein CSE002 Lab06 part B 
// October 11, 2018

import java.util.Scanner;

public class PatternB {
  public static void main (String[] args){
    Scanner myScanner = new Scanner (System.in);
    boolean given = false; //used to initiate loop
 // introduces variables
    int numbers = 0; //int variable for number of numbers on the starting line
    int lines = 0; //int variable for number of lines, intitializes variable
    System.out.println ("Please enter an integer 1-10 to decide how many rows to create."); //prompts user
    //Makes sure the user inputs an int variable
    while (given==false){ //initiates loop
     if (myScanner.hasNextInt()){ //
       lines = myScanner.nextInt ();//user inputs how many lines they want for the pattern
       given = true;
    }
      else { // When if statement isn't true this will prompt the user again and ask for an int
        System.out.println ("Error");
        System.out.println ("You did not enter an integer, please now enter an integer 1-10. ");
        myScanner.next();
      }
    }
    int length = lines ;
    for(int numRows = 1; numRows <= lines; numRows++){ //For loop which establishes number of lines
      for ( numbers = 1; numbers <= length; numbers++){ //For loop which determines number of lines printed
       System.out.print (numbers + " ");//prints out each row of numbers
      //
      
      }
      System.out.println ();
      length--;
    }
  }
}