// Josh Feinstein CSE002 Lab09
// Objectives. This lab will illustrate the effect of passing arrays as method arguments.
// Laboratory Session 9: Passing Arrays Inside Methods

import java.util.*;
public class PassingArrays{
public static void main (String[]args){ 
int[] array0 = {1,2,3,4,5,6,7,8};
int[] array1 = copy(array0);
  int[] array2 = copy(array0);
 invert(array0);
  print(array0);
  invert2(array1);
  print(array1);
    int[] array3 =   invert2(array2);
  print (array3);

  
}

public static int[] copy(int[] a0){
int[] a1 = new int [a0.length];
for (int i = 0; i < a0.length; i++){
a1[i] = a0[i];  
}  
  return a1;
}
public static void invert(int[] a0){
 int [] a1 = copy(a0);
  for (int i = 0; i<a0.length;i++ ){
    a0[i] = a1[a0.length - i - 1];
  }
}
public static int[] invert2(int[] a0){
  int[] a2 = copy(a0);
  for (int i = 0; i< a2.length/2; i++){
    a2[i] = a2[a2.length - i - 1];
  }
  return a2;
}
  public static void print(int[] a0){
    for (int i = 0; i<a0.length; i++){
      int k = a0[i];
      System.out.print(k + " ");
    }
    System.out.println();
  }
  
}