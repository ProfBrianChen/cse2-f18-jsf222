//Josh Feinstein CSE002 Lab06 part C 
// October 11, 2018
// Program will:
// 1. Ask for input
// 2. Use input to create that many lines
import java.util.Scanner;

public class PatternC {
  public static void main (String[] args){
   boolean given = false;
    int r = 1; //declares variable
Scanner myScanner = new Scanner (System.in);
    System.out.println ("Please enter an integer 1-10 to decide how many rows to create.");
        while (given==false){ //initiates loop
     if (myScanner.hasNextInt()){ //
       r = myScanner.nextInt ();//user inputs how many lines they want for the pattern
       given = true;
    }
      else { // When if statement isn't true this will prompt the user again and ask for an in
        System.out.println ("Error");
        System.out.println ("You did not enter an integer, please now enter an integer 1-10. ");
        myScanner.next();
      }
    }

for (int i = 1; i <= r; i++) { //iterate from 1 to input  
    //Loop from i+1 to insert spaces first
    for (int j = i+1; j <= r; j++) {
        System.out.print(" ");
    }
    //Loop from i to insert the number next to each other
    for (int j = i; j >= 1; j--) {
        System.out.print(j + "");
    }
    System.out.println(); //insert a new line
}
  }
}