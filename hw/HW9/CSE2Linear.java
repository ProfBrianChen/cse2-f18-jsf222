// Josh Feinstein CSE002 HW09 Final

import java.util.*;

public class CSE2Linear{ //class
 
  public static void main(String[] args){ //main method
    Scanner scn = new Scanner (System.in); //creates new scanner
    final int a1[] = new int [15]; //declares array
    System.out.println("Enter 15 ascending ints for final grades in CSE2:"); //prompts user for input values
    for (int i = 0; i < 15;i++){ //for statement for inputting all individual values
          boolean given = false; //Boolean variable for testing if integer

      while (given == false){ //While loop to ensure user inputs an integer
        if (scn.hasNextInt()){
          a1[i] = scn.nextInt(); //inputs value for position on array
          given = true; //leaves while loop
        }
        else {
          System.out.println("Please make sure you enter an int.");
          scn.next();
        }
      }
      
      while (a1[i] > 100 || a1[i] < 0){ //While loop for ensuring values are between 0 and 100
        System.out.println("Please make sure value is between 0 and 100.");
          a1[i] = scn.nextInt();
      }
      if (i >0){
      while (a1[i] < a1[i - 1]){
       System.out.println("Please make sure the values are in ascending order.");
        a1[i] = scn.nextInt();
        }
      }
      
      
       }
    //for loop to print out array
    for (int i = 0; i<15; i++){
      int k = a1[i];
      System.out.print(k + " ");
     }
    
    System.out.println();
    
    System.out.println("Enter a grade to search for:"); //Asks user to input a grade to search for
    binarySearch(a1); //uses binary search method to search for grade
    scramble(a1); //uses scramble method to rearrange the order of the grades in the array
    System.out.print("Scrambled: "); //prints out scrambled array
    
     for (int i = 0; i<15; i++){ //for loop to print out array
      int k = a1[i];
      System.out.print(k + " ");
     }
    
    
    System.out.println("Enter a grade to search for:"); //Asks user to input a grade to search for

    linearSearch(a1); //uses linear search method to search for grade
    
  }
  
  
  public static void binarySearch(int[] a2){ //binary search method
    Scanner scn = new Scanner (System.in);
    int grade = scn.nextInt(); //variable for what grade is being searched for 
    int iterations = 0; //how many iterations the binary search took to find the number
    int low = 0; //lowest value in array
    int high = 14; //highest value in array
    while (low <= high){
      int mid = (high + low)/2;
      if (grade == a2[mid]){
        iterations++;
        System.out.println(grade + " was found with " + iterations + " iterations.");
      }
      
      if (grade < a2[mid]){
        high = mid - 1;
        iterations++;
      }
      
      else {
        low = mid + 1;
        iterations++;
      }
    }
  }
  
  
  public static int[] scramble(int[] a1){
    Random r = new Random ();
    
    for (int i = 0; i < 15; i++){ //asseses each value at each part of the array
      int p = r.nextInt(15);
     int t = a1[i];
      a1[i] = a1[p];
      a1[p]= t;
       }
    return a1;
  }
  
  public static void linearSearch(int[] a1){
    Scanner scn = new Scanner (System.in);
    int grade = scn.nextInt(); //variable for what grade is being searched for 
    int iterations = 0; //how many iterations the binary search took to find the number
    for (int i = 0; i < 15; i++){
      iterations++;
      if (a1[i] == grade){
        System.out.println(grade + " was found with " + iterations + " iterations.");
      }
    }
  }
  
}