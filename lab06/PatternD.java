//Josh Feinstein CSE002 Lab06 part D
// October 11, 2018
// Program will:
// 1. Ask for input
// 2. Use input to create that many lines
import java.util.Scanner;

public class PatternD {
  public static void main (String[] args){
    boolean given = false;
    int r = 1;
  
Scanner myScanner = new Scanner (System.in);
    System.out.println ("Please enter an integer 1-10 to decide how many rows to create."); //asks user how many lines they want
        while (given==false){ //initiates loop
     if (myScanner.hasNextInt()){ //
       r = myScanner.nextInt ();//user inputs how many lines they want for the pattern
       given = true;
    }
      else { // When if statement isn't true this will prompt the user again and ask for an int
        System.out.println ("Error");
        System.out.println ("You did not enter an integer, please now enter an integer 1-10. ");
        myScanner.next();
      }
    }
int k = r; //establishes variable which can be adjusted without changing int r
for (int i = 1; i <= r; i++) { //iterate from 1 to input  
   
    for (int j = k; j >=1; j--) { //inserts each number next to each other
        System.out.print(j + " ");
   
    }
    
    
    System.out.println(); //insert a new line
  k--; //Decreases the starting number on the next line
}
  }
}