//Josh Feinstein CSE002
//HW08
//


import java.util.*;
public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } 
  public static void printArray(String[]cards){
    for (int i=0; i<cards.length; i++){ //For loop which goes through each variable in the array
      System.out.print(cards[i] + " "); //prints out String value at poistion i of the array
    }
    System.out.println();
    
  }
  public static void shuffle(String[] cards){
    Random rn = new Random(); //creates a Random variable
    for (int i = 0; i <52; i++){
      int rand = i + rn.nextInt(52-i); //Randomizes which card is where
      String x = cards[rand]; //Has string be equal to the new random position in array
      cards [rand] = cards[i]; //changes the order of the positions on array
      cards [i] = x;
    }
    System.out.println("Shuffled");
  }
  public static String[] getHand(String[] cards, int index, int numCards){
    String[] hand = new String[numCards];
    System.out.println("Hand");
    for (int i = 0; i<numCards; i++){ //for loop for counting through the deck
      hand[i] = cards[index - i]; //draws the number at the end of the array
      
    }
    return hand;
  }
}
